import threading

class BankAccount(object):
    def __init__(self):
        self.lock = threading.Lock()
        self.balance = 0
        self.is_open = False

    def get_balance(self):
        if self.is_open is False:
            raise ValueError("Account is closed")
        else:
            return self.balance

    def open(self):
        if self.is_open is True:
            raise ValueError("Account is already open")
        else:
            self.balance = 0
            self.is_open = True

    def deposit(self, amount):
        if self.is_open is False:
            raise ValueError("Account is closed")
        if (amount) < 0:
            raise ValueError("Cannot withdraw a negative number")
        with self.lock:
            self.balance += amount

    def withdraw(self, amount):
        if self.is_open == False:
            raise ValueError("Account is closed")
        if (self.balance - amount) < 0:
            raise ValueError("Account balance cannot be < 0")
        if (amount) < 0:
            raise ValueError("Cannot withdraw a negative number")
        with self.lock:
            self.balance -= amount

    def close(self):
        if self.is_open is False:
            raise ValueError("Account is closed.")
        else:
            self.is_open = False
            self.balance = 0
