#!/usr/bin/env python3

from boto3 import client
from datetime import datetime, timedelta, timezone
from dateutil.tz import tzutc

def lc(max_age,iam,users):
    # list comprehension
    users_older_than_60_days = [u for u in users if max_age >= u['CreateDate']]
    print(users_older_than_60_days)

def adbmal(max_age,iam,users):
    # lambda version to understand Python's lambda
    users_oldr_than_60_days = list(filter(lambda x: max_age >= x['CreateDate'], users))
    print(users_oldr_than_60_days)

def main():

    max_age = (datetime.now(timezone.utc) - timedelta(days=60))

    # TODO  generate list of users with access key > 90 days
    #       write list to an SQLite file

    iam = client('iam')

    paginator = iam.get_paginator('list_users')
    for paginated_resp in paginator.paginate():
        users = [user for user in paginated_resp['Users']]

    lc(max_age,iam,users)

    adbmal(max_age,iam,users)

main()
